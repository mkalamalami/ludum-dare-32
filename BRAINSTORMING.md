## 774 	Companion

* MINI BROTHERS : Jeu d'exploration en vue RPG 2D où l'on contrôle deux joueurs à la fois, un à la souris et l'autre au clavier. Ils doivent s'entraider pour progresser (a priori juste des touches de déplacement, éventuellement le clic pour le perso à la souris). Accessoirement jouable à deux si chacun prend un perso.
* SPEED DATING : Jeu idiot de rapidité. Lors d'une soirée speed-dating, il faut avoir du succès sur un maximum de rencontres en utilisant différentes touches du clavier pour avoir les mêmes caractéristiques/mimiques que le personnage d'en face (S pour ajuster le sourire, L pour changer de lunettes, etc.). Au final c'est une sorte de monsieur patate mais le thème peut rendre les choses marrantes.

## 696 	An Unconventional Weapon

* LA BRUTE : Un peu comme "La Brute", mais en solo, avec des niveaux à franchir, et au lieu du hasard on doit programmer un enchaînement de coups et de mouvements avec des armes diverses, que le combattant répète en boucle. Le but étant de s'adapter à l'enchaînement de l'ennemi en face, pour bloquer/se déplacer/attaquer avec une arme appropriée à chaque action. Le thème serait intégré via l'histoire, selon laquelle les combattants se confrontent en tournoi pour remporter une arme bizarre ?
* GUNCRAFT : Action-platformer où l'on rassemble des pièces détachées pour fabriquer des flingues de plus en plus gros et compliqués. La moitié du jeu consiste à buter des créatures et trouver des pièces, l'autre à assembler des armes dans un éditeur. Mode édition = on pose une coque qui définit l'espace disponible pour des composants, puis on pose un ou plusieurs chargeurs automatiques/manuels, remplis avec tel ou tel type de munitions, et relié à un ou plusieurs canons, etc.

## 568 	Deeper and Deeper

* BROKEN MINE : Un mineur descend de plus en plus profondément dans une mine (il se déplace automatiquement). Le but est à la fois de déblayer/réparer le chemin devant lui, et de le faire se baisser/sauter au fil des obstacles irréparables qu'on rencontre.
* UNE SEULE MANETTE : Un platformer super difficile avec un seul niveau, où à chaque fois qu'on réussit le niveau on doit le recommencer, mais il devent un peu plus long (généré aléatoirement ?). Le trick : une seule personne peut y jouer à la fois, les autres sont spectateurs et il y a une file d'attente pour jouer. Dès qu'on meurt/finit le niveau, on donne la main et on retourne au bout de la file d'attente. Idéalement complété par un chat où à côté de chaque nom de joueur s'affiche le nombre de mètres qu'il a réussi à parcourir au total avant chaque mort. Et un leaderboard des mètres parcourus.
* LIFE : Jeu basé sur un moteur physique, où l'on contrôle une étoile qui a pour objectif de rassembler de la poussière en une planète à l'aide de la gravité, puis de l'entretenir en la gardant à une distance idéale et en provoquant des collisions avec des comètes chargées d'eau par exemple, tout en les protégeant d'astéroïdes destructeurs en les absorbant directement dans le soleil.

## 432 	Grow

* UNE SEULE MANETTE (le fait que le niveau grossisse)

## 372 	Adapt to Survive

* LA BRUTE

## 212 	Among the Stars

* WHERE IS WILL SMITH? : Court RPG dans un château où se déroule une fête pour les 50 ans de Will Smith. Tous les acteurs/réalisateurs/etc. ayant travaillé avec lui à un moment ou un autre dans sa carrière sont présents. Grâce à plein de données tirées d'IMDB, on génère des NPCs avec des noms réels ; quand on leur parle, on peut leur demander d'où ils connaissent Will Smith (ex. "j'ai joué dans tel film avec lui"). Histoire courte où Will Smith lui même a disparu, le joueur est à sa recherche.
* LIFE

## 194 	It Spreads

* LA TARTINE : Mini-jeu de rapidité/précision où il faut tartiner des choses, sur différentes formes de pain, avec différents ustensiles (= formes de pinceaux), parfois sur plusieurs couches ("une tartine nutella-beurre de cacahuètes-schamallows s'il vous plaît"). Temps limité et fixe, score selon précision du travail.

## 149 	You are the Power Source

* LIFE (soleil = power source)
* TELEKINESIE : Puzzle-platformer où l'on joue un petit personnage dans un niveau fait de grands volumes. On peut à tout moment passer en mode télékinésie en restant appuyé sur une touche, de manière à faire se déplacer un volume avec lequel on est en contact (sol/mur/plafond) à l'aide des flèches. Tant que le mode est activé, le personnage reste "soudé" au volume. Faire se déplacer les volumes permet d'accéder à des endroits hors d'atteinte par de simples sauts, etc.

## 107 	Edge of the World
## 79 	Hidden World
## 12 	Four Elements

* LA TARTINE (les 4 éléments : nutella / confiture / beurre de cacahuètes / crème de schamallows ?)
* LIFE (feu du soleil, eau des comètes, terre issue de la poussière... et vent ? grâce aux marées apportées en donnant une lune à la Terre ?)

## -6 	Discovery
## -44 	Creatures of the Night
## -91 	Indirect Control

* BROKEN MINE (variante où l'on ne contrôle pas directement le joueur pour sauter/se baisser, par exemple en posant des torches pour lui signaler les obstacles à éviter ?)

## -141 	Day and Night
## -141 	Harvest
## -149 	Take One, Leave the Rest

* SPEED DATING... ?

## -153 	Abandoned
## -161 	Self-Destruction
## -166 	Hunted/Hunter

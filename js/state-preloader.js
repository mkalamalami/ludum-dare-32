define(['assets', 'consts', 'game-state'], function(Assets, Consts, GameState) {

	/**
	 * Preloading screen.
	 * Loads all assets, then launches the actual game.
	 */

	var state = new Phaser.State();

	state.preload = function () {
		GameState.load();
		GameState.level = Consts.DEBUG.INITIAL_LEVEL; // Don't save level
		GameState.lastLevel = GameState.level-1;

		this.game.input.gamepad.start();

		this.game.input.keyboard.addKeyCapture(Phaser.Keyboard.SPACEBAR);
		this.game.input.keyboard.addKeyCapture(Phaser.Keyboard.UP);
		this.game.input.keyboard.addKeyCapture(Phaser.Keyboard.DOWN);
		this.game.input.keyboard.addKeyCapture(Phaser.Keyboard.LEFT);
		this.game.input.keyboard.addKeyCapture(Phaser.Keyboard.RIGHT);

		this.ready = false;

		//	Load assets
		this.background = this.add.sprite(this.world.centerX, this.world.centerY - 100, 'preloader_background');
		this.background.anchor.set(.5);
		this.preloadBar = this.add.sprite(this.world.centerX, this.world.centerY + 150, 'preloader_bar');
		this.preloadBar.anchor.set(.5);

		//	Set the preloadBar sprite as a loader sprite.
		this.load.setPreloadSprite(this.preloadBar);

		preloadImageFolder('img/', Assets.images);
		preloadAudioFolder('audio/', Assets.audio);
		preloadSpritesheetFolder('img/spritesheet/', Assets.spritesheet);

		// preload physics
		state.load.physics('physicsData', 'img/shapes.json');
	};

	state.create = function () {

		//	Disable the crop once the load has finished, let the music decode
		this.preloadBar.cropEnabled = false;

	};

	state.update = function () {
		// Wait until the music is decoded for a smoother transition
		 if ((this.cache.isSoundDecoded('music-theme1') || Consts.DEBUG.SKIP_WAIT_MP3_DECODING) && this.ready == false)
		 {
			this.ready = true;
			var text = this.game.add.text(this.game.width/2, this.game.height/2+50, 'Click anywhere to begin');
			text.addColor('#FFFFFF', 0);
			text.setStyle({font: Consts.FONT});
			text.anchor.x = text.anchor.y = .5;
			text.alpha = 0.
			var game = this.game;
			game.add.tween(text).to({alpha: 1.}, 300).start();
			if (Consts.DEBUG.SKIP_CLICK_TO_START) {
				game.state.start('cutscene');
			}
			else {
				this.game.input.onDown.add(function() {
					game.state.start('cutscene');
				});
			}
		}
	};

	return state;

	// ===== PRELOADERS  =====

	function preloadImageFolder(folderName, assets) {
		assets.forEach(function(asset) {
			if (typeof asset == 'string') {
				var assetPath = folderName + asset + ((asset.indexOf('.') == -1) ? '.png' : '');
				state.load.image(asset, assetPath);
			}
			else if (typeof asset == 'object') {
				preloadImageFolder(folderName + asset[0], _.tail(asset));
			}
			else {
				console.error("Invalid asset type: " + (typeof asset));
				console.error(asset);
			}
		});
	}

	function preloadAudioFolder(folderName, assets) {
		assets.forEach(function(asset) {
			if (typeof asset == 'string') {
				var assetPath = folderName + asset + ((asset.indexOf('.') == -1) ? '.mp3' : '');
				state.load.audio(asset, [assetPath], true);
			}
			else if (typeof asset == 'object') {
				preloadAudioFolder(folderName + asset[0], _.tail(asset));
			}
			else {
				console.error("Invalid asset type: " + (typeof asset));
				console.error(asset);
			}
		});
	}

	function preloadSpritesheetFolder(folderName, assets) {
		assets.forEach(function(asset) {
			var assetPath = folderName + asset[0] + ((asset[0].indexOf('.') == -1) ? '.png' : '');
			state.load.spritesheet(asset[0], assetPath, asset[1], asset[2]);
		});
	}
});

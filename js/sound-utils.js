define(['consts', 'game-state'], function(Consts, GameState) {

	return {
		playMusic: playMusic,
		setMute: setMute
	};

	var currentMusic;
	var mute = GameState.mute;

	function playMusic(game, id, volume) {
		var newMusic = 'music-' + id;
		if (!currentMusic || currentMusic.name != newMusic) {
			if (currentMusic) currentMusic.stop() &&
				console.log(currentMusic.name, id);
			currentMusic = game.sound.play(newMusic, volume || 1, true);
			setMute(mute);
		}
		return currentMusic;
	}

	function setMute(newMute) {
		if (currentMusic) {
			if (newMute) {
				currentMusic.oldVolume = currentMusic.volume;
				currentMusic.volume = 0.;

			}
			else {
				currentMusic.volume = currentMusic.oldVolume || 1;
			}
		}
		mute = newMute;
	}

});

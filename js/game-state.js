define(['consts'], function(Consts) {

	var gameState = {};

	gameState.init = function() {
		_.each(gameState, function(value, key) {
			if (typeof (value) != "function") {
				delete gameState[key];
			}
		});

		// Initial data
		gameState.level = Consts.DEBUG.INITIAL_LEVEL;
		gameState.lastLevel = gameState.level-1;
	};

	gameState.save = function(otherName) {
		var data = {};
		_.each(gameState, function(value, key) {
			if (typeof (value) != "function") {
				data[key] = value;
			}
		});
		var gameName = otherName || gameState.gameName || 'save';
		$.jStorage.set(gameName, JSON.stringify(data));

		var savedGames = $.jStorage.get("savedGames");
		if (savedGames == undefined) {
			savedGames = [];
		}
		savedGames = _.without(savedGames, gameName);
		savedGames.unshift(gameName);
		$.jStorage.set("savedGames", savedGames);
	};

	gameState.removeSavedGame = function(gameName) {
		var savedGames = $.jStorage.get("savedGames");
		savedGames = _.without(savedGames, gameName);
		$.jStorage.set("savedGames", savedGames);
		$.jStorage.deleteKey(gameName);
	};

	gameState.findExistingGames = function(gameName) {
		var savedGames = $.jStorage.get("savedGames");
		if (savedGames == undefined) {
			savedGames = [];
		}
		return savedGames;
	};

	gameState.load = function(gameName) {
		gameName = gameName || 'save';
		var rawData = $.jStorage.get(gameName);
		if (rawData != null) {
			var data = JSON.parse(rawData);
			_.each(data, function(value, key) {
				gameState[key] = value;
			});
			gameState.date = moment(gameState.date);
		}
		return rawData != null;
	};

	return gameState;

});

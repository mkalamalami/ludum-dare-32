define(['consts'], function(Consts) {

return {

	images: [
		'cutscene',
		'black',
		'mute',
		'restart',
		'stars',
		'missile',
		'cloud',
		'white',
		[ 'icon/',
			'ok',
			'no',
			'arrow1'
		],
		[ 'block/',
			'1-1-1','1-1-2','1-1-3','1-1-4','1-1-5','1-1-6',
			'1-2','1-3','1-4','1-5','1-6','1-7','1-8',
			'2-1','2-2','2-3','2-4','2-5','2-7','2-9',
			'3-1','3-2','3-3',
			'4-1','4-2','4-3',
			'5-1','5-3',
			'6-1','6-2',
			'7-1','7-2',
			'8-1','8-2',
			'9-1','9-2',
			'10-1','10-2',
			'11-1',
			'15-1',
			'26-1',
			'30-1',
			'laser'
		],
		['bg/',
			'level0',
			'level1',
			'level2',
			'level3',
			'level4',
			'level5',
			'level6',
			'level7'
		]
	],
	audio: [
		'run',
		'use-power',
		'landing',
		//'fall',
		'note1',
		'note2',
		'note3',
		'note4',
		'note5',
		'music-theme1',
		'music-space',
		'music-ending',
		'rockslide',
		'explosion'
	],
	spritesheet: [
		['menu_new',170,30],
		['menu_load',170,30],
		['player',Consts.PLAYER_SIZE,Consts.PLAYER_SIZE],
		['robot',48,96],
		['explosion',96,96]
	]

};

});

define(['assets', 'consts', 'game-state', 'sound-utils'], function(Assets, Consts, GameState, SoundUtils) {

	var state = new Phaser.State();

	var bg, bgShader;

	state.preload = function () {
		state.cutscenes = startCutscene(GameState.level, function() {
			startLevel();
		});
	};

	state.create = function () {
		if (!GameState.mute) {
			if (GameState.level >= Consts.LEVEL_ENDING) {
				this.music = SoundUtils.playMusic(this.game, 'ending', .6);
			}
			else if (GameState.level >= Consts.LEVEL_STARS) {
				this.music = SoundUtils.playMusic(this.game, 'space', .5);
			}
			else {
				this.music = SoundUtils.playMusic(this.game, 'theme' + ((GameState.level % Consts.THEME_COUNT) + 1));
			}
		}

	};

	state.update = function () {
		if (this.music) this.music.update();
		if (bgShader) {
			bgShader.uniforms.fadeOut.value = bg.fadeOut;
			bgShader.update();
		}
	};

	function startLevel() {
		if (GameState.level != Consts.LEVEL_ENDING+1) {
			state.game.state.start('level');
		}
		else {
			state.game.state.start('credits');
		}
	}

	function startCutscene(id, callback) {
		if (Consts.DEBUG.SKIP_CUTSCENES || GameState.lastLevel > GameState.level) {
			startLevel();
			return;
		}

		var game = state.game;

        // Background
        bg = state.add.sprite(0, 0);
        bg.width = game.width;
        bg.height = game.height;
		bg.filters = [getBgFilter(game)];

        // Text
        var text = getCutsceneText(id);
		if (!text) {
			startLevel();
			return;
		}
		text = text.split('\n');
        var lineId = 0;
		var delay = 1000;
        text.forEach(function(line) {
			var filteredLine = line.replace(/DELAY/g, '');
            var lineSprite = game.add.text(
					bg.width/2, state.game.height/2 - 25*text.length + 40*lineId, filteredLine);
			lineSprite.anchor.set(.5);
			lineSprite.addColor('#FFFFFF', 0);
			lineSprite.setStyle({font: Consts.FONT});
            lineSprite.alpha = 0.;
			state.add.tween(lineSprite)
				.to({ }, delay)
				.to({ alpha: 1. }, 1000, Phaser.Easing.Quadratic.In)
				.start();
			var delayBonus = (line.length - filteredLine.length)/5;
			delay += 800 + filteredLine.length * 50 + delayBonus * 500;
            lineId++;
        });

		// Fade in
		var fadeSprite = state.add.sprite(0, 0, 'black');
		fadeSprite.width = game.width;
		fadeSprite.height = game.height;
		fadeSprite.alpha = 1.;
		state.add.tween(fadeSprite).to({ alpha: 0. }, 1000).start();

		// Fade out and start level
		bg.fadeOut = 0.;
		var endTween = state.add.tween(bg);
		endTween.to({ }, delay + 1000);
		endTween.start();
		endTween.onComplete.add(function() {
			var fadeOutTween = state.add.tween(fadeSprite);
			fadeOutTween.to({ alpha: 1. }, 1500, Phaser.Easing.Quadratic.In).start();
			fadeOutTween.onComplete.add(function() {
				setTimeout(callback, 500);
			})
		});

	}

    function getCutsceneText(id) {
        if (id == 0) {
            return "My head is spinning...DELAY\nI'm not sure where they put me.\n\nBut my powers are starting to come back.\nIt's time to get out."
        }
	    if (id == Consts.LEVEL_STARS) {
            return "Well...\n\nI'm further from home than I thought."
	    }
	    if (id == Consts.LEVEL_ENDING+1) {
            return "A breach.DELAY\n\nThey used me as a weapon, to open a breach.\nThey may have won against the machines -\n\nBut that's not how it ends."
	    }
    }

	function getBgFilter(game) {
		if (!bgShader) {
			var sprite = game.add.sprite(0, 0, 'cutscene');
			sprite.visible = false;

			bgShader = new Phaser.Filter(game, {
				fadeOut: {type: '1f', value: 0.},
				iChannel0: {
					type: 'sampler2D',
					value: sprite.texture,
					textureData: { repeat: true }
				}
			}, document.getElementById('bg-shader').innerHTML.split('\n'));
		}
		return bgShader;
	}

	return state;

});

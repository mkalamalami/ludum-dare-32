define(['consts'], function(Consts) {

	return {
		createButton: createButton
	};

	// Main menu button
	function createButton(state, x, y, text, callback, options) {
		// available options:
		// centered=true
		// size=true
		// sprite=...
		options = options || {};

		var button = state.add.group();

		button.background = state.add.sprite(0, 0, options.sprite || 'gui-long');
		if (options.centered) {
			button.background.anchor.set(0.5);
		}
		button.background.inputEnabled = true;
		button.background.input.useHandCursor = true;
		button.background.events.onInputDown.add(callback, state);
		button.add(button.background);

		button.label = state.add.text(
			(options.centered) ? 0 : background.width / 2,
			(options.centered) ? 0 : background.height / 2,
			text,
			{font: 'bold ' + (options.size || 20) + 'pt Arial', fill: 'white'});
		button.label.align = 'center';
		button.label.anchor.set(0.5);
		button.add(button.label);

		button.x = x;
		button.y = y;

		return button;
	}
	
});

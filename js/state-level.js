define(['consts', 'game-state', 'entities/tilemap', 'entities/player', 'entities/monster', 'sound-utils'],
		function(Consts, GameState, FactoryTilemap, FactoryPlayer, FactoryMonster, SoundUtils) {

    var state = new Phaser.State();

	var overlayShader;

    state.preload = function() {
			this.game.physics.startSystem(Phaser.Physics.P2JS);
			this.game.physics.p2.setImpactEvents(true);
			this.game.physics.p2.restitution = 0;
			this.game.physics.p2.gravity.x = 0;
			this.game.physics.p2.gravity.y = 1000;
			this.game.physics.p2.friction = 0;
			this.game.physics.p2.applyFriction = false;
			this.game.physics.p2.applySpringForces = false;
			this.game.physics.p2.applyDamping = false;

			this.game.stage.backgroundColor = '#0a1012';
			this.load.tilemap('level' + GameState.level, 'tilemaps/maps/level' + GameState.level + '.json', null, Phaser.Tilemap.TILED_JSON);
			//this.load.spritesheet('tmp-tileset', 'tilemaps/tiles/tmp-tileset.png', 32, 32);

		}

    state.create = function() {

			if (!GameState.mute) {
				if (GameState.level >= Consts.LEVEL_STARS) {
					this.music = SoundUtils.playMusic(this.game, 'space', .5);
				}
				else {
					this.music = SoundUtils.playMusic(this.game, 'theme' + ((GameState.level % Consts.THEME_COUNT) + 1));
				}
			}

			this.levelInfo = {};

			var stars = this.game.add.sprite(0, 0, 'stars');
			stars.fixedToCamera = true;

			FactoryTilemap.load(this, 'level' + GameState.level);
			this.player = FactoryPlayer.create(state, this.levelInfo);

			this.player.body.setCollisionGroup(state.levelInfo.playerCollisionGroup);
			this.player.body.collides(state.levelInfo.blocksCollisionGroup);
			this.player.body.collides(state.levelInfo.textCollisionGroup);
			if (!Consts.DEBUG.INVINCIBLE) {
				this.player.body.collides(state.levelInfo.monsterCollisionGroup, function(){this.kill();}, this.player);
				this.player.body.collides(state.levelInfo.deathCollisionGroup, function(){this.kill();}, this.player);
			}

			var monstersGroup = state.add.group();
			this.levelInfo.monsters.forEach(function(data) {
				var monster = FactoryMonster.create(state, data.x, data.y, data.direction ||'left');
				monster.body.mass = 0.001;
				monster.body.setCollisionGroup(state.levelInfo.monsterCollisionGroup);
				monster.body.collides(state.levelInfo.blocksCollisionGroup);
				monster.body.collides(state.levelInfo.monsterLimitCollisionGroup);
				monster.body.collides(state.levelInfo.playerCollisionGroup);
				if (!Consts.DEBUG.INVINCIBLE) {
					monster.body.collides(state.levelInfo.deathCollisionGroup, function(){this.kill();}, monster);
				}
				monstersGroup.add(monster);
			});

			this.game.physics.p2.updateBoundsCollisionGroup();

			this.electricity = createElectricity(this.game);

			this.muteButton = this.game.add.button(this.game.width - 60, this.game.height - 60, 'mute', this.toggleMute, this);
			this.muteButton.fixedToCamera = true;
			this.toggleMute();
			this.toggleMute();

			var player = this.player;
			this.restartButton = this.game.add.button(this.game.width - 120, this.game.height - 60, 'restart', function(){player.kill()});
			this.restartButton.fixedToCamera = true;
			this.restartButton.alpha = .8;

			createOverlay(this.game);
    };

    state.update = function() {
		overlayShader.update();
		this.electricity.update();

		// Anim player death
		if (!this.player.alive || this.player.won !== undefined) {
			overlayShader.uniforms.darkness.value += .05;
			if (overlayShader.uniforms.darkness.value > 1) {
				if (this.player.won !== undefined && this.player.alive) {
					GameState.lastLevel = GameState.level;
					GameState.level = this.player.won;
					this.game.state.start('cutscene');
				}
				else {
					this.game.state.start('level');
				}
			}
		}
		else if (overlayShader.uniforms.darkness.value > .01) {
			overlayShader.uniforms.darkness.value -= .05;
		}

		if (GameState.level == Consts.LEVEL_ENDING && this.player.x > 1500 && !this.ending) {
			var missile = this.add.sprite(2500, 300, 'missile');
			this.ending = function() {
				missile.x -= 7;
				if ((state.game.time.time*100) % 6 == 0) {
					this.ending.clouds.push(state.game.add.sprite(missile.x + 280, missile.y - 40, 'cloud'));
				}
				this.ending.clouds.forEach(function(cloud) {
					if (cloud.alpha > 0) cloud.alpha -= .02;
					if (cloud.alpha == 0) cloud.destroy();
				});
				if (missile.x < 300 && !missile.light) {
					var light = state.game.add.sprite(0,0,'white');
					light.width = state.game.width;
					light.height = state.game.height;
					light.fixedToCamera = true;
					light.alpha = .3;
					missile.light = light;
				}
				if (missile.light) {
					if (missile.light.alpha < 1) {
						missile.light.alpha += .03;
					}
					else {
						GameState.level = Consts.LEVEL_ENDING+1;
						state.game.state.start('cutscene');
					}
				}
			};
			this.ending.clouds = [];
		}
		if (this.ending) {
			this.ending();
		}
    };

    state.render = function() {
    };

	state.shutdown = function(){
		FactoryPlayer.destroy(this.player);
	};

	state.toggleMute = function() {
		GameState.mute = !GameState.mute;
		SoundUtils.setMute(GameState.mute);
		GameState.save();
		this.muteButton.alpha = GameState.mute ? .2 : .8;
	}


	/*
		 Be careful, do not call this method before resizing the world
	*/
	state.initCollisionGroups = function(){
		this.levelInfo.blocksCollisionGroup = this.game.physics.p2.createCollisionGroup();
		this.levelInfo.monsterCollisionGroup  = this.game.physics.p2.createCollisionGroup();
		this.levelInfo.playerCollisionGroup  = this.game.physics.p2.createCollisionGroup();
		this.levelInfo.monsterLimitCollisionGroup  = this.game.physics.p2.createCollisionGroup();
		this.levelInfo.deathCollisionGroup  = this.game.physics.p2.createCollisionGroup();
		this.levelInfo.textCollisionGroup  = this.game.physics.p2.createCollisionGroup();
	}

	function createElectricity() {
		var electricity = state.game.add.sprite(0, 0);
		electricity.width = state.game.width;
		electricity.height = state.game.height;
		electricity.visible = false;
		electricityShader = new Phaser.Filter(state.game, {
			active: {type: '1f', value: 0.},
			spritePos: {type: '2f', value: {x:0.,y:0.}},
			spriteSize: {type: '2f', value: {x:0.,y:0.}},
			playerPos: {type: '2f', value: {x:0.,y:0.}}
		}, document.getElementById('electricity-shader').innerHTML.split('\n'));
		electricityShader.setResolution(state.game.width, state.game.height);
		electricity.filters = [electricityShader];
		electricity.update = function() {
			var playerBlock = state.player.highlightedBlock;
			if (electricity.highlightedBlock != playerBlock) {
				electricity.highlightedBlock = playerBlock;
				if (playerBlock) {
					electricity.visible = true;
					electricityShader.uniforms.spriteSize.value = {x: playerBlock.width, y: playerBlock.height};
				}
				else {
					electricity.visible = false;
				}
			}
			if (electricity.highlightedBlock) {
				electricityShader.uniforms.active.value = state.player.selectedBlock ? 1. : 0.;
				electricityShader.uniforms.playerPos.value = {x: state.player.x-state.game.camera.x, y: state.player.y-state.game.camera.y};
				electricityShader.uniforms.spritePos.value = {x: electricity.highlightedBlock.x-state.game.camera.x, y: electricity.highlightedBlock.y-state.game.camera.y};
			}
			electricityShader.update();
		}
		return electricity;
	}

	function createOverlay(game) {
		var overlay = game.add.sprite(0, 0);
		overlay.width = game.width;
		overlay.height = game.height;
		overlayShader = new Phaser.Filter(game, {
			darkness: {type: '1f', value: 1.}
		}, document.getElementById('leveloverlay-shader').innerHTML.split('\n'));
		overlay.filters = [overlayShader];
	}

    return state;

});

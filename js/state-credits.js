define([ 'consts' ], function(Consts) {

	var state = new Phaser.State();

	state.preload = function() {
	},

	state.create = function() {

		var t = this.game.add.text(this.game.width/2, this.game.height/2, 'Thanks for playing!');
		confText(t);
		t = this.game.add.text(this.game.width/2, this.game.height/2+100, 'Programming: Wan, Manu  -  Graphics: Jack, Wan  -  Level design: Manu  -  Audio: Wan');
		confText(t, Consts.FONT_LEVEL);
	};

	function confText(text, font) {
		text.anchor.x = .5;
		text.anchor.y = .5;
		text.addColor('#FFFFFF', 0);
		text.setStyle({font: font || Consts.FONT});
	}

	return state;

});

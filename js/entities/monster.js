define(['consts', 'physics-utils'], function(Consts, PhysicsUtils) {
	return {
		create: create
	};

	function create(state, x, y, direction) {
		var monster = state.game.add.sprite(x, y, 'robot');
		monster.mode = "walk";
		monster.objectType = "monster";
		monster.direction = direction;

		// Animation
		monster.animations.add('walk');
		monster.animations.play('walk', 15, true);

		// Physics
		state.game.physics.p2.enable(monster, Consts.DEBUG.VISUAL);
		// monster.body.x += monster.width/2; // (X.X)
		monster.body.y -= monster.height/4;
		monster.body.mass = .01;
		monster.body.fixedRotation = true;
		monster.body.fixed = true;
		monster.gravity = 0;
		monster.body.clearShapes();
		monster.body.loadPolygon('physicsData', 'alien');
		// Audio

		// IA
		monster.update = function() {
			if (PhysicsUtils.isStuck(state.game, monster)) {
				PhysicsUtils.explosion(state.game, monster);
				monster.kill();
				return;
			}

			monster.collisionBlock == null;
			if (monster.direction == "left") {
				monster.collisionBlock = PhysicsUtils.findContact(state.game, monster, [-1,0]);
				monster.body.velocity.x -= 40;
			}
			else if (monster.direction == "right") {
				monster.collisionBlock = PhysicsUtils.findContact(state.game, monster, [1,0]);
				monster.body.velocity.x += 40;
			}

			if(monster.collisionBlock) {
				// Turn back
				monster.body.velocity.x = 0;
				if (monster.direction == "left") {
            		monster.animations.play('right');
					monster.direction = "right";
				}
				else if (monster.direction == "right") {
					monster.animations.play('left');
					monster.direction = "left";
				}
			}
			monster.body.velocity.x *= .8;
		};
		return monster;
	}
});

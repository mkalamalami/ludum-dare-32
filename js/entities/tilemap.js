define(['consts', 'physics-utils', 'game-state'], function(Consts, PhysicsUtils, GameState) {

	return {
		load: load
	};

	/**
	Object types:
	- block [default] (magnet: bool, locked: bool)
	- player
	- monster (type: string, direction: left/right)
	- exit
	*/

  function load(state, tilemapId) {
		// Load map
		var map = state.add.tilemap(tilemapId);
		//map.addTilesetImage('tmp-tileset', 'tmp-tileset');
		//layer = map.createLayer('background');

		var levelBg = state.game.add.image(0, 0, 'level' + GameState.level);
		// Be careful, do not call this method after initializing collision groups
		state.game.world.setBounds(0, 0, levelBg.width, levelBg.height);

		// init collision groups
		state.initCollisionGroups();

      // Init blocks groups
      var blocksGroup = state.add.group();
      var ceilingBlocksGroup = state.add.group();

      // Create blocks
      var objectsLayer = map.objects["objects"];
			// var levelInfo = {};
			state.levelInfo.monsters = new Array();
			objectsLayer.forEach(function(object) {
				if (object.type === "player" && (parseInt(object.properties.from) + GameState.level == GameState.lastLevel || object.properties.from == 0 || !object.properties.from)) {
					state.levelInfo.playerPos = {
						x: object.x - Consts.PLAYER_SIZE/2,
						y: object.y - Consts.PLAYER_SIZE
					};
				}
				else if (object.type == "text") {
					var textBody = state.add.sprite(object.x, object.y);
					textBody.width = object.width;
					textBody.height = object.height;
					state.game.physics.p2.enable(textBody);
					PhysicsUtils.fixBody(textBody);
					textBody.objectType = "text";
					textBody.body.setCollisionGroup(state.levelInfo.textCollisionGroup);
					textBody.body.collides(state.levelInfo.playerCollisionGroup);
					textBody.body.static = true;
					//text.addColor('#FFFFFF', 0);
					textBody.body.onBeginContact.addOnce(function(body) {
						var text = state.add.text(0,0, object.properties.text ||"???", {font: Consts.FONT_LEVEL});
						text.alpha = 0.;
						text.addColor('#FFFFFF', 0);
						var tween = state.game.add.tween(text)
							.to({alpha: 1.}, 500)
							.to({}, 1000 + 50*text.text.length)
							.to({alpha: 0.}, 1000)
							.start();
						body.sprite.text = text;
						textBody.kill();
					});
				}
				else if (object.type == "exit") {
					var exit = state.add.sprite(object.x, object.y);
					exit.visible = false;
					exit.width = object.width;
					exit.height = object.height;
					state.game.physics.p2.enable(exit);
					PhysicsUtils.fixBody(exit);
					exit.objectType = "exit";
					exit.level = GameState.level + parseInt(object.properties.to);
					exit.body.setCollisionGroup(state.levelInfo.blocksCollisionGroup);
					exit.body.collides(state.levelInfo.blocksCollisionGroup);
					exit.body.collides(state.levelInfo.playerCollisionGroup);
					exit.body.static = true;
					exit.body.onBeginContact.add(function(body) {
						if (body.sprite.objectType == "player") {
							body.sprite.won = exit.level;
						}
					});
				}
				else if (object.type == "block" || (!object.type && object.width)) {
					var x = Math.round(object.x * 1. / Consts.TILE_SIZE) * Consts.TILE_SIZE;
					var y = Math.round(object.y * 1. / Consts.TILE_SIZE) * Consts.TILE_SIZE;
					var ti = Math.round(object.width * 1. / Consts.TILE_SIZE);
					var tj = Math.round(object.height * 1. / Consts.TILE_SIZE);

					var sprite;
					if (object.properties.magnet) {
						sprite = createBlockSprite(state, x, y, ti, tj, true);
						sprite.magnet = true;
						sprite.update = function() {
							// Make it static again when it touches something
							sprite.body.applyForce([0,Consts.GRAVITY/200], sprite.body.x, sprite.body.y);
							if (!sprite.body.static	&& !sprite.grabbedByPlayer
								&& PhysicsUtils.findContact(state.game, sprite, [0,1], 'block')) {
								if (!sprite.body.readyToGetStatic) {
									if (!GameState.mute)
									state.game.sound.play('use-power', .1, false);
									sprite.body.readyToGetStatic = 0;
								}
								sprite.body.readyToGetStatic++;
								if (sprite.body.readyToGetStatic > 10) {
									sprite.body.static = true;
									delete sprite.body.readyToGetStatic;
								}
							}


							// Rock slide sound
							if (sprite.grabbedByPlayer) {
								if (!sprite.playingSlide) {
									if (!GameState.mute) sprite.playingSlide = state.game.sound.play('rockslide', .5, true);
								}
								else {
									sprite.playingSlide.volume = .5;
								}
							}
							else if (sprite.playingSlide) {
								sprite.playingSlide.stop();
								delete sprite.playingSlide;
							}

							// Fall up if support is lost FIXME
						/*	if (sprite.body.static && !PhysicsUtils.findContact(state.game, sprite, [0,1])) {
								PhysicsUtils.makeNonStatic(sprite);
							}*/
						}
						ceilingBlocksGroup.add(sprite);
					}
					else {
						sprite = createBlockSprite(state, x, y, ti, tj);
						sprite.update = function() {
							// Make it static again when it touches something
							if (!sprite.body.static && !sprite.grabbedByPlayer
								&& PhysicsUtils.findContact(state.game, sprite, [0,-1], 'block')) {
								if (!sprite.body.readyToGetStatic) {
									if (!GameState.mute) state.game.sound.play('use-power', .1, false);
									sprite.body.readyToGetStatic = 0;
								}
								sprite.body.readyToGetStatic++;
								if (sprite.body.readyToGetStatic > 10) {
									sprite.body.static = true;
									delete sprite.body.readyToGetStatic;
								}
							}

							// Rock slide sound
							if (sprite.grabbedByPlayer) {
								if (!sprite.playingSlide) {
									if (!GameState.mute) sprite.playingSlide = state.game.sound.play('rockslide', .5, true);
								}
								else {
									sprite.playingSlide.volume = .5;
								}
							}
							else if (sprite.playingSlide) {
								sprite.playingSlide.stop();
								delete sprite.playingSlide;
							}

							// Fall down if support is lost FIXME
							/*if (sprite.body.static && !PhysicsUtils.findContact(state.game, sprite, [0,-1])) {
								PhysicsUtils.makeNonStatic(sprite);
							}*/
						}
						blocksGroup.add(sprite);
					}
					state.game.physics.p2.enable(sprite);
					PhysicsUtils.fixBody(sprite);
					sprite.defaultMass = sprite.body.mass;
					sprite.objectType = "block";
					sprite.locked = object.properties.locked;
					sprite.body.setCollisionGroup(state.levelInfo.blocksCollisionGroup);
					sprite.body.collides(state.levelInfo.blocksCollisionGroup);
					sprite.body.collides(state.levelInfo.playerCollisionGroup);
					sprite.body.collides(state.levelInfo.monsterCollisionGroup);
				} else if(object.type == "monster") {
					var monster = {};
					monster.x = object.x;
					monster.y = object.y;
					monster.objectType = object.type;
					monster.direction = object.properties.direction;
					state.levelInfo.monsters.push(monster);
				} else if(object.type == "monsterLimit") {
					var limit = state.add.sprite(object.x, object.y);
					limit.visible = false;
					limit.width = object.width;
					limit.height = object.height;
					state.game.physics.p2.enable(limit);
					limit.body.static = true;
					limit.body.setCollisionGroup(state.levelInfo.monsterLimitCollisionGroup);
					limit.body.collides(state.levelInfo.monsterCollisionGroup);
				} else if(object.type == "death") {
					var death = state.add.sprite(object.x, object.y, object.properties.type);
					death.width = object.width;
					death.height = object.height;
					if (object.properties.hidden) {
						death.visible = true;
					}
					state.game.physics.p2.enable(death);
					death.body.static = true;
					death.body.setCollisionGroup(state.levelInfo.deathCollisionGroup);
					death.body.collides(state.levelInfo.monsterCollisionGroup);
					death.body.collides(state.levelInfo.playerCollisionGroup);
					PhysicsUtils.fixBody(death);
				}
			});

      // Set up physics
			// ceilingBlocksGroup.setAll('body.immovable', true);

			blocksGroup.setAll('body.gravity.y', Consts.GRAVITY);
			blocksGroup.setAll('body.static', true);
			blocksGroup.setAll('body.fixedRotation', true);

			ceilingBlocksGroup.setAll('body.fixedRotation', true);
			ceilingBlocksGroup.setAll('body.static', true);

			blocksGroup.add(ceilingBlocksGroup);
			state.levelInfo.blocksGroup = blocksGroup;
			// state.levelInfo.blocksCollisionGroup = blocksCollisionGroup;
			return state.levelInfo;

  }

	function createBlockSprite(state, x, y, ti, tj, /*opt*/magnet) {
		var GAP = 0;
		var spriteName = ti + '-' + tj;
		if (ti == 1 && tj == 1) {
			spriteName += '-' + Math.ceil(Math.random()*6);
		}
		sprite = state.game.add.sprite(x + GAP, y + GAP, spriteName);
		sprite.width = Consts.TILE_SIZE * ti - GAP*2;
		sprite.height =Consts.TILE_SIZE * tj - GAP*2;
	    attachBlockShader(state, sprite, magnet);
	    return sprite;
	}

	// TODO Remove?
	function attachBlockShader(state, sprite, /*opt*/magnet) {
  		var uniforms = {
  		/*	iChannel0: { type: 'sampler2D', value: sprite.texture, textureData: { repeat: true }},*/
  			spriteSize: { type: '2f', value: { x: sprite.width, y: sprite.height } },
				magnet: {type: '1f', value: (magnet?1:0)}
			};

  		var blockShader = new Phaser.Filter(state.game, uniforms,
			document.getElementById('block-shader').innerHTML.split('\n'));
  		blockShader.setResolution(state.game.width, state.game.height);
  		//filters.push(blockShader);
  		sprite.filters = [ blockShader ];
	}

});

define(['consts', 'physics-utils', 'game-state'], function(Consts, PhysicsUtils, GameState) {

	return {
		create: create,
		destroy: destroy
	};

	function create(state, levelInfo) {
		var player = state.game.add.sprite(levelInfo.playerPos.x, levelInfo.playerPos.y, 'player');
		player.mode = "walk";
		player.objectType = "player";

		// Animation
		player.animations.add('walk',  [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]);
		player.animations.add('jump', [5,6,7,6]);
		player.animations.play('walk', 10, true);

		// Physics
		state.game.physics.p2.enable(player, Consts.DEBUG.VISUAL);
		player.body.x += player.width/2; // (X.X)
		player.body.y += player.height/2;
		player.body.mass = .005;
		player.body.fixedRotation = true;
		player.body.clearShapes();
		player.body.loadPolygon('physicsData', 'player');
		state.game.camera.follow(player);

		// Audio
		player.powerSound = state.game.add.audio('use-power');
		player.powerSound.volume = .6;
		player.runSound = state.game.add.audio('run');
		player.runSound.volume = .05;
		player.landingSound = state.game.add.audio('landing');
		//player.fallSound = state.game.add.audio('fall');
		//player.fall=false;
		player.lastHighlightPlayed = 0;

		// Input
		player.update = function() {
			// Kill
			/*if (PhysicsUtils.isStuck(state.game, player)) {
				if (!player.dying) player.dying = 0;
				player.dying++;
			}
			else {
				delete player.dying;
			}*/
			if (!Consts.DEBUG.INVINCIBLE && (PhysicsUtils.hitsObjectType(state.game, player, 'monster')
				|| player.dying > 3)) {
				PhysicsUtils.explosion(state.game, player);
				 player.kill();
				return;
			}

			// Move text
			if (player.text) {
				player.text.x = player.x - player.text.width / 2;
				player.text.y = player.y - 50;
			}


			var gamepadState = {
				x: state.game.input.gamepad.pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X),
				y: state.game.input.gamepad.pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y),
				power: state.game.input.gamepad.pad1.isDown(Phaser.Gamepad.XBOX360_RIGHT_TRIGGER)
					|| state.game.input.gamepad.pad1.isDown(Phaser.Gamepad.XBOX360_RIGHT_BUMPER)
					|| state.game.input.gamepad.pad1.isDown(Phaser.Gamepad.XBOX360_X),
				jump: state.game.input.gamepad.pad1.isDown(Phaser.Gamepad.XBOX360_A),
			}
			
			// Detect highlighted block
			if (player.mode == "walk") {
				var newHighlightedBlock = null;
				if (state.game.input.keyboard.isDown(Phaser.Keyboard.LEFT) || gamepadState.x < 0) {
					newHighlightedBlock = PhysicsUtils.findContact(state.game, player, [-1,0], 'block');
				}
				else if (state.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) || gamepadState.x > 0) {
					newHighlightedBlock = PhysicsUtils.findContact(state.game, player, [1,0], 'block');
				}
				else if (state.game.input.keyboard.isDown(Phaser.Keyboard.UP) || gamepadState.y < 0) {
					newHighlightedBlock = PhysicsUtils.findContact(state.game, player, [0,1], 'block');
				}
				else {
					newHighlightedBlock = PhysicsUtils.findContact(state.game, player, [0,-1], 'block');
				}
				if (newHighlightedBlock === true) {
					newHighlightedBlock = null;
				}
				if (newHighlightedBlock
					&& player.lastTouchedBlock != newHighlightedBlock
				 	&& state.game.time.time > player.lastHighlightPlayed + 200) {
					if (player.lastHighlightPlayed) {
						if (!GameState.mute) state.game.sound.play('note' + Math.ceil(Math.random()*Consts.NOTE_COUNT), .5)
					}
					player.lastHighlightPlayed = state.game.time.time;
				}
				player.highlightedBlock = newHighlightedBlock;
				if (newHighlightedBlock) {
					player.lastTouchedBlock = newHighlightedBlock;
				}
			}

			// Mode toggle input
			if (player.mode == "walk"
				&& (state.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) || gamepadState.power)) {
				player.selectedBlock = player.highlightedBlock;
				if (player.selectedBlock && player.selectedBlock.body != "undefined") {
					player.mode = "fly";
					if (!GameState.mute) player.powerSound.play();
					player.body.setZeroVelocity();
					player.selectedBlock.grabbedByPlayer = true;
					PhysicsUtils.makeNonStatic(player.selectedBlock);
					player.waitForRelease = ((state.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) || gamepadState.x > 0) ? Phaser.Keyboard.RIGHT : false);
					if (state.game.input.keyboard.isDown(Phaser.Keyboard.LEFT) || gamepadState.x < 0) {
						player.waitForRelease = Phaser.Keyboard.LEFT;
					}
					player.lockConstraint = state.game.physics.p2.createLockConstraint(player.body, player.selectedBlock.body,
						[player.body.x-player.selectedBlock.body.x,
						 player.body.y-player.selectedBlock.body.y]);
				}

			}
			else if (player.mode == "fly"
					&& !state.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && !gamepadState.power) {
				player.mode = "walk";
				state.game.physics.p2.removeConstraint(player.lockConstraint);
				player.body.setZeroVelocity();
				if (player.selectedBlock) {
					player.selectedBlock.grabbedByPlayer = false;
					player.selectedBlock.body.setZeroVelocity();
					player.selectedBlock = null;
				}
			}

			// Animation
			var canJump = PhysicsUtils.canJump(state.game, player);
			if (canJump) {
				if (player.animations.currentAnim.name == 'jump') {
					player.animations.play('walk', 10, true);
				}
				delete player.jumpCharge;
			}
			else if (player.animations.currentAnim.name == 'walk') {
				if (!player.jumpCharge) player.jumpCharge = 0;
				player.jumpCharge++;
				if (player.jumpCharge > 5) {
					player.animations.play('jump', 5, true);
				}
			}

			// Fly mode input
			if (player.waitForRelease
					&& !state.game.input.keyboard.isDown(player.waitForRelease)
					&& ((player.waitForRelease == Phaser.Keyboard.LEFT) ? gamepadState.x >= 0 : gamepadState.x <= 0)) {
				player.waitForRelease = false;
			}

			var speed = {
				x: Consts.PLAYER_SPEED, y: Consts.PLAYER_SPEED
			}
			if (gamepadState.x || gamepadState.y) {
				speed.x = Math.abs(gamepadState.x * 100);
				speed.y = Math.abs(gamepadState.y * 100);
			}

			if (player.mode == "fly" && player.selectedBlock && !player.waitForRelease) {
				if (state.game.input.keyboard.isDown(Phaser.Keyboard.LEFT)
					  || gamepadState.x < 0) {
					player.selectedBlock.body.velocity.x -= speed.x * 100/ Consts.PLAYER_SPEED;
				}
				else if (state.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)
					  || gamepadState.x > 0) {
					player.selectedBlock.body.velocity.x += speed.x * 100/ Consts.PLAYER_SPEED;
				}
				if (state.game.input.keyboard.isDown(Phaser.Keyboard.UP)
					  || gamepadState.y < 0) {
					player.selectedBlock.body.velocity.y -= speed.y * 100/ Consts.PLAYER_SPEED;
				}
				else if (state.game.input.keyboard.isDown(Phaser.Keyboard.DOWN)
					  || gamepadState.y > 0) {
					player.selectedBlock.body.velocity.y += speed.y * 100/ Consts.PLAYER_SPEED;
				}
				player.selectedBlock.body.velocity.x *= .8;
				player.selectedBlock.body.velocity.y *= .8;
				player.body.setZeroVelocity();
			}

			// Walk mode input
			else if (player.mode == "walk") {

				if (state.game.input.keyboard.isDown(Phaser.Keyboard.LEFT)
					  || gamepadState.x < 0) {
					if (player.body.velocity.x > 0){
						player.body.velocity.x *= .5;
					}
					player.scale.x = -1;
					player.body.velocity.x -= speed.x;
					if(!player.runSound.isPlaying && player.body.velocity.x < -100 && canJump){
						if (!GameState.mute) player.runSound.play("",0,undefined,true);
					}
				}
				else if (state.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)
					  || gamepadState.x > 0) {
					if (player.body.velocity.x < 0){
						player.body.velocity.x *= 0.5;
					}
					if (state.game.input.gamepad.pad1.connected) {

					}
					player.body.velocity.x += speed.x;
					player.scale.x = 1;
					if(!player.runSound.isPlaying && player.body.velocity.x > 100 && canJump){
						if (!GameState.mute) player.runSound.play("",0,undefined,true);
					}

				}
				if ((state.game.input.keyboard.isDown(Phaser.Keyboard.UP) || gamepadState.jump)
					&& canJump && player.body.velocity.y > -10) {
					player.body.velocity.y -= 460;
				}
				player.body.velocity.x *= .8;
				if(Math.abs(player.body.velocity.x) < 100 || Math.abs(player.body.velocity.y) > 20){
					if(player.runSound.isPlaying){
						player.runSound.stop();
					}
				}

				/*if(player.fall && !player.fallSound.isPlaying && player.body.velocity.y > 700){
					player.gameOver();
				}
				if(!player.fall && player.body.velocity.y > 400){
					player.fall = true;
				} else if(canJump && player.fall) {
					player.landingSound.play();
					player.fallSound.stop();
					player.fall = false;
				}*/
			}
		};

		/*player.gameOver = function() {
			player.fallSound.play();
		};*/
		return player;
	}

		function destroy(player) {
			player.runSound.stop();
		};
});

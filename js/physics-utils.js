define(['consts', 'game-state'], function(Consts, GameState) {

	return {
		canJump: canJump,
		isStuck: isStuck,
		hitsObjectType: hitsObjectType,
		findContact: findContact,
		findAnyContact: findAnyContact,
		fixBody: fixBody,
		makeNonStatic: makeNonStatic,
		areTouching: areTouching,
		explosion: explosion
	};

	// yes explosions are physics
	function explosion(game, sprite) {
		var exp = game.add.sprite(sprite.x - sprite.width/2-24,
			sprite.y - sprite.height/2-24,
			'explosion');
		if (!GameState.mute) game.sound.play('explosion', .4);
		exp.alpha = .7;
		exp.animations.add('boom');
		exp.animations.play('boom', 20, false);
	}

	function makeNonStatic(sprite) {
		if (sprite.body) {
			sprite.body.static = false;
			sprite.body.mass = .1;
			sprite.body.setZeroVelocity();
		}
	}

	function canJump(game, sprite) {
		return findContact(game, sprite, [0,-1]) != false;
	}

	function isStuck(game, sprite) {
		return findContact(game, sprite, [-1,0]) && findContact(game, sprite, [1,0])
			|| findContact(game, sprite, [0,-1]) && findContact(game, sprite, [0,1])
	}

	function areTouching(game, spriteA, spriteB) {
		var result = false;
		game.physics.p2.world.narrowphase.contactEquations.forEach(function (c) {
			if (c.bodyA === spriteA.body.data || c.bodyB === spriteA.body.data) {
				if (c.bodyA === spriteB.body.data || c.bodyB === spriteB.body.data) {
					result = true;
				}
			}
		});
		return result;
	}

	function hitsObjectType(game, sprite, objectType) {
		for (var i = 0; i < game.physics.p2.world.narrowphase.contactEquations.length; i++) {
			var c = game.physics.p2.world.narrowphase.contactEquations[i];
			if (c.bodyA === sprite.body.data || c.bodyB === sprite.body.data) {
				if (c.bodyB.parent != null && c.bodyA.parent != null) {
					var otherSprite = c.bodyA === sprite.body.data ? c.bodyB.parent.sprite : c.bodyA.parent.sprite;
					if (otherSprite.objectType == objectType) {
						return true;
					}
				}
			}
		}
		return false;
	}

	function findAnyContact(game, sprite, /*opt*/objectType) {
		var result = false;
		game.physics.p2.world.narrowphase.contactEquations.forEach(function(c) {
			if (c.bodyA === sprite.body.data || c.bodyB === sprite.body.data) {
				if(c.bodyB.parent != null && c.bodyA.parent != null) {
					result = c.bodyA === sprite.body.data ? c.bodyB.parent.sprite : c.bodyA.parent.sprite;
					if (objectType && result.objectType != objectType) {
						result = false;
					}
				}
			}
		});
		return result;
	}

	function findContact(game, sprite, direction /* [1 0 ou -1,1 0 ou -1] */, /*opt*/objectType) {
		var axis = p2.vec2.fromValues(direction[0], -direction[1]);
		var result = false;
		for (var i = 0; i < game.physics.p2.world.narrowphase.contactEquations.length; i++)
		{
				var c = game.physics.p2.world.narrowphase.contactEquations[i];

				if (c.bodyA === sprite.body.data || c.bodyB === sprite.body.data) {
						var d = p2.vec2.dot(c.normalA, axis);
						if (c.bodyA === sprite.body.data) d *= -1;
						if (d > 0.5) {
							if(c.bodyB.parent != null && c.bodyA.parent != null){
								result = c.bodyA === sprite.body.data ? c.bodyB.parent.sprite : c.bodyA.parent.sprite;
								if (objectType && result.objectType != objectType) {
									result = false;
								}
							} else {
								return true;
							}

						}
				}

				if (result) {
					break;
				}
		}
		return result;
	}

	function fixBody(sprite) {
		// (X.X)
		sprite.body.x += sprite.width/2;
		sprite.body.y += sprite.height/2;
		return sprite;
	}

});

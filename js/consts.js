define([], function() {

	return {

		CANVAS_ID : 'phaser',

		DEBUG : {
			SKIP_CLICK_TO_START: true,
			SKIP_CUTSCENES: true,
			SKIP_WAIT_MP3_DECODING: true,
			VISUAL: false,
			INITIAL_LEVEL: 0,
			INVINCIBLE: false
		},
		/*{
			SKIP_CLICK_TO_START: false,
			SKIP_CUTSCENES: false,
			SKIP_WAIT_MP3_DECODING: false,
			VISUAL: false,
			INITIAL_LEVEL: 0,
			INVINCIBLE: false
		},*/

		MIN_WIDTH : 800,
		MIN_HEIGHT : 600,
		MAX_WIDTH : Math.min(1200,window.innerWidth) - 30, // 960,
		MAX_HEIGHT : Math.min(600,window.innerHeight) - 30,  // 600,

		TILE_SIZE: 48, /*px*/
		PLAYER_SIZE: 48, /*px*/
		SPRITESHEET_SIZE: 30, /*tiles*/

		PLAYER_SPEED: 80,
		GRAVITY: 2050,

		NOTE_COUNT: 5,
		THEME_COUNT: 1,
		LEVEL_STARS: 5,
		LEVEL_ENDING: 7,

		FONT: "25pt 'VT323', monospace",
		FONT_LEVEL: "13pt 'VT323', monospace"
	};

});


	require(['../lib/underscore', '../lib/json2', '../lib/jstorage', '../lib/moment'], function() {

		require(['consts', 'state-boot', 'state-preloader', 'state-level', 'state-cutscene', 'state-credits'],
			function(Consts, StateBoot, StatePreloader, StateLevel, StateCutscene, StateCredits) {
			var game = new Phaser.Game(Math.max(Consts.MIN_WIDTH,Consts.MAX_WIDTH), Math.max(Consts.MIN_HEIGHT,Consts.MAX_HEIGHT), Phaser.AUTO, 'phaser', {
				create: function() {
					game.state.add('boot', StateBoot);
					game.state.add('preloader', StatePreloader);
					game.state.add('cutscene', StateCutscene);
					game.state.add('level', StateLevel);
					game.state.add('credits', StateCredits);

					game.state.start('boot');
				}
			});

		});
	});
